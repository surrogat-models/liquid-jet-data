#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 16:12:05 2021

@author: dkreuter
"""

# SCRIPT TO TURN SMILEI RESULTS INTO THE FORMAT USED BY THE NEURAL NETS

print("Importing Modules...")
import sys
sys.path.append("/Smilei")
import happi
import numpy as np
import scipy.constants as const
import argparse
import csv

# List of constants
epsilon0    = const.epsilon_0                    # Vaccuum permittivity
me          = const.m_e                          # Electron mass
mp          = const.m_p                          # Proton mass
e           = const.e                            # Elemantary charge
c0          = const.c                            # Speed of liexit
mu0         = const.mu_0                         # Vacuum permeability

print("...Finished Importing Models")

# Loading data from simulation

print("Loading Simulation.....")

S     = happi.Open()
frontdist1=S.namelist.frontdist
plasmathick1=S.namelist.plasmathick
gamma0=S.namelist.gamma0
theta=S.namelist.theta
refl        = c0/(S.namelist.omegal*gamma0)                         # SMILEI reference length c/omegaL
refn        = epsilon0*me*((S.namelist.omegal*gamma0)**2.)/(e**2.)   # SMILEI reference particle density

# Select all particles located behind the target and which are moving to the right
eleclo= S.TrackParticles(species='electrons', axes = ["x", "px", "py", "pz", "w"], sort="any(t>0, (px>0)*(x>"+str((frontdist1+plasmathick1))+")*(w>0))", sorted_as='behind_target').getData()
olo= S.TrackParticles(species='oxygen', axes = ["x", "px", "py", "pz", "w"], sort="any(t>0, (px>0)*(x>"+str((frontdist1+plasmathick1))+")*(w>0))", sorted_as='behind_target').getData()
plo= S.TrackParticles(species='hydrogen', axes = ["x", "px", "py", "pz", "w"], sort="any(t>0, (px>0)*(x>"+str((frontdist1+plasmathick1))+")*(w>0))", sorted_as='behind_target').getData()
dlo= S.TrackParticles(species='deuterium', axes = ["x", "px", "py", "pz", "w"], sort="any(t>0, (px>0)*(x>"+str((frontdist1+plasmathick1))+")*(w>0))", sorted_as='behind_target').getData()

print("... Finished Loading Simulation")
print("Preparing Data...")

def ekindirect(pdata, m):
    # Compute the kinetic energy directly from the particle momenta in the simulation frame in smilei units
    px = pdata['px'].flatten()
    py = pdata['py'].flatten()
    pz = pdata['pz'].flatten()
    #en = np.zeros((px.shape[0]))
    #for i in range(px.shape[0]):
    #    en[i] = (gamma0*me*c0**2 * (np.sqrt((px[i]**2+py[i]**2+pz[i]**2) + m**2/(me**2)) + np.sin(theta)*py[i]) - m*c0**2)/e
    # vectorized for loop:
    en = (gamma0*me*c0**2 * (np.sqrt((px**2+py**2+pz**2) + m**2/(me**2)) + np.sin(theta)*py) - m*c0**2)/e
    return en

def fillbins2(energies, binwidth, binnumber, lorentzdict):
    # Go through each energy entry and place it in the appropriate bin
    filled = np.zeros((binnumber))
    weights = lorentzdict['w'].flatten()
    for i in range(len(energies)):
        if int(energies[i]/binwidth)<binnumber:
            index = int(energies[i]/binwidth)
            filled[index] = filled[index] + weights[i]/(gamma0**2) * refl*refn
    return filled/binwidth

u = const.physical_constants['atomic mass constant'][0]

print("Calculating Energy...")

enions = ekindirect(olo, 15.999*u - 8*me)

enelec = ekindirect(eleclo, me)

enpro = ekindirect(plo, mp)
endeu = ekindirect(dlo, mp)

# Define 100 bins in [0,1]

num_bins  = 100
bin_width = 1/num_bins
bins = [(i*bin_width, (i+1)*bin_width) for i in range(num_bins)]

# Normalise energy list to [0,1]

h_ekin_norm = enpro/np.amax(enpro)
d_ekin_norm = endeu/np.amax(endeu)
o_ekin_norm = enions/np.amax(enions)
e_ekin_norm = enelec/np.amax(enelec)

# Fill bins with log(counts)

print("Binning...")

h_ekin_binned = np.log(fillbins2(h_ekin_norm, bin_width, num_bins, plo))
d_ekin_binned = np.log(fillbins2(d_ekin_norm, bin_width, num_bins, dlo))
o_ekin_binned = np.log(fillbins2(o_ekin_norm, bin_width, num_bins, olo))
e_ekin_binned = np.log(fillbins2(e_ekin_norm, bin_width, num_bins, eleclo))

# Load sample-parameters and append parameter+output line to ML-ready CSV

parser = argparse.ArgumentParser("Which sample is being processed?")
parser.add_argument("-n", help="index of sample", default=0, type=int)
args = parser.parse_args()

samples = np.loadtxt("../sampled_input_parameters.txt")

h_line = np.append(samples[args.n], np.append(h_ekin_binned, np.amax(enpro)*1e-6))
d_line = np.append(samples[args.n], np.append(d_ekin_binned, np.amax(endeu)*1e-6))
o_line = np.append(samples[args.n], np.append(o_ekin_binned, np.amax(enions)*1e-6))
e_line = np.append(samples[args.n], np.append(e_ekin_binned, np.amax(enelec)*1e-6))

print("...Finished Data Post-Processing")
print("Writing into CSV")

with open('../../inputoutput-H.csv', 'a') as Hf:
    writer = csv.writer(Hf)
    writer.writerow(h_line)

with open('../../inputoutput-D.csv', 'a') as Df:
    writer = csv.writer(Df)
    writer.writerow(d_line)

with open('../../inputoutput-O.csv', 'a') as Of:
    writer = csv.writer(Of)
    writer.writerow(o_line)

with open('../../inputoutput-e.csv', 'a') as ef:
    writer = csv.writer(ef)
    writer.writerow(e_line)

print("DONE")
