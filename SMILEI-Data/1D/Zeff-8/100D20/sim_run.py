# -*- coding: utf-8 -*-
"""
Created on Wed Aug 18 17:43:44 2021

@author: dkreu
"""

# SCRIPT TO INITIALISE UP TO 10000 SIMULATIONS ON VIRGO
# Usage is: For n simulations into queue: "python sim_run.py -n"
# All file paths are the actual file paths that were used on Virgo

#import numpy as np
import os
import argparse
from datetime import date, datetime

parser = argparse.ArgumentParser("Generate some samples and run simulations!")
parser.add_argument("-n", help="number of samples and simulations", default=100, type=int)
parser.add_argument("-outfile", help="name of sample output .npy file", default="sampled_input_parameters")

args = parser.parse_args()
N = args.n # number of simulations

# Make new directory for simulations and generate list of all the sampled parameters
now=datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H-%M-%S")
simdir = str(dt_string)+"_"+str(N)
os.makedirs(simdir)
os.chdir(simdir)
os.system("singularity exec /lustre/bhs/bschmitz/smilei.sif python3 ../make_samples.py -n "+str(args.n)+" -outfile "+str(args.outfile))
os.system("export LUSTRE_HOME=/lustre/$(id -g -n)/$USER")
os.system("export PATH=$LUSTRE_HOME/bin:$PATH")
os.system("sbatch --array=0-"+str(N-1)+"%500 --chdir=/lustre/bhs/bschmitz/simulations/"+simdir+" -- /lustre/bhs/bschmitz/simulations/array_batch.sh "+simdir)
