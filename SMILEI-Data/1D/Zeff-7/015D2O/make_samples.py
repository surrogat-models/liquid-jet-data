# -*- coding: utf-8 -*-
"""
Created on Wed Aug 11 16:03:14 2021

@author: dkreuter
"""

# SAMPLING SCRIPT FOR THE VIRGO SIMULATIONS - CALLED BY sim_run.py

# -------------------
# IMPORT MODULES
# -------------------
import numpy as np
import argparse
from numpy.random import uniform
import pint
import scipy.constants as const


ureg = pint.UnitRegistry(system='cgs')
me          = const.electron_mass           # Electron mass
mp          = const.proton_mass             # Proton mass
e           = const.elementary_charge       # Elemantary charge
c0          = const.speed_of_light          # Speed of light
epsilon0    = const.epsilon_0               # Vacuum permittivity
ne_w        = 6.68e28                       # Liquid jet water part. density

#np.random.seed(seed=10) # random seed for re-producability

# -------------------
# Functions
# -------------------
        
def geometryfactor(FWHM):
    """
    function to convert a FWHM value to the 2 sigma area
    Assumption is the cylinder symmetry of the laserpulse
    """
    import numpy as np
    # r = (np.sqrt(2/np.log(2))*FWHM)/2
    r = FWHM/np.sqrt(2*np.log(2))
    return np.pi*r**2

def E0fromE(tau, L, E):
    """
    function to obtain E0 from E, tau, L

    """
    const = 1.37e18 * ureg.watt / ureg.centimeter**2 * ureg.micrometer**2 * ureg.elementary_charge**2 / 4 / np.pi**2 / ureg.speed_of_light**4 / ureg.electron_mass**2
    return np.sqrt(E/(tau*geometryfactor(L)*const))

def omega(wavelength):
    """
    obtain radial frequency from wavelength
    """
    return 2*np.pi*c0/wavelength



# -------------------
# Parameter Ranges
# -------------------

# Parameter ranges according to Table in Thesis/paper
Parameter_Dict = {
    'Energy':(1E-3 ,5E+1)* ureg.joule,
    'Focus-FWHM':(2.,20.)* ureg.micrometer,
    #'Pulselength':(15.,150.)* ureg.femtosecond, # continous for small interval
    'Pulselength':[15.,50., 93.,130., 171.,249., 327., 405.,483.,562., 640., 718., 796., 874., 952., 1031., 1109., 1187., 1265., 1343., 1421.,1500.,]*ureg.femtosecond,
    'Polarisation':['s','p'],
    'Theta_L':(0., 85. )* ureg.degree,
    'Contrast_L':(1E-6,5E-12), #unused
    'lambda_L':np.arange(550. ,1100.,50.)* ureg.nanometer,
    'd_T':[0.6, 0.8, 1.0, 1.5, 2.0, 3.0, 3.5, 4.0, 5.0, 5.5, 6.0, 8.0, 10.0 ] * ureg.micrometer,
    'Element':['H2O', 'D2O'] #unused in the randome sampling, but part of global "mixture" variable.
}

omega_L = (2*np.pi*ureg.speed_of_light/Parameter_Dict['lambda_L']).to_base_units()# radians per second

# -------------------
# Sampling
# -------------------


parser = argparse.ArgumentParser("Generate some samples!")
parser.add_argument("-n", help="number of samples", default=100, type=int)
parser.add_argument("-outfile", help="name of output file", default="samples")

args = parser.parse_args()

N_SAMPLES = args.n
#N_SAMPLES = 500000

# Sampling Parameters

# Initially, the Buckingham-Pi Theorem was used to find dimensionless quantities to describe the system.
# these Pis are still mentioned here but were not used in the actual work.
# Only relevant: Pi 4e = a0 of the laser

param_combis = np.zeros((N_SAMPLES, 13))

param_headings = ["Energy", "Focus-FWHM", "Pulselength", "Polarisation", "Theta_L", "lambda_L", "Target Thickness", "Pi 1", "Pi 2", "Pi 3e", "Pi 4e", "Pi 4O", "Pi 4p"]
# Only sqrt of E is relevant in the Pis, so sampling is done uniformly in the sqrt-domain and is squared again afterwards to be correct in joule
param_combis[:, 0] = uniform(np.sqrt(Parameter_Dict["Energy"][0].magnitude), np.sqrt(Parameter_Dict["Energy"][-1].magnitude), size=N_SAMPLES)**2
param_combis[:, 1] = uniform(Parameter_Dict["Focus-FWHM"][0].to('meter').magnitude, Parameter_Dict["Focus-FWHM"][-1].to('meter').magnitude, size=N_SAMPLES)
#param_combis[:, 2] = uniform(Parameter_Dict["Pulselength"][0].to('second').magnitude, Parameter_Dict["Pulselength"][-1].to('second').magnitude, size=N_SAMPLES)
param_combis[:, 2] = np.random.choice(Parameter_Dict["Pulselength"].to('second').magnitude, size=N_SAMPLES)
# For polarisation: 0 = s, 1 = p
param_combis[:, 3] = np.random.choice([0,1], size=N_SAMPLES)
param_combis[:, 4] = uniform(Parameter_Dict["Theta_L"][0].magnitude, Parameter_Dict["Theta_L"][-1].magnitude, size=N_SAMPLES)
param_combis[:, 5] = np.random.choice(Parameter_Dict["lambda_L"].to('meter').magnitude, size=N_SAMPLES)
param_combis[:, 6] = np.random.choice(Parameter_Dict["d_T"].to('meter').magnitude, size=N_SAMPLES)
param_combis[:, 7] = omega(param_combis[:, 5])*param_combis[:, 2]
param_combis[:, 8] = omega(param_combis[:, 5])*param_combis[:, 1]/c0
param_combis[:, 9] = e**2 * ne_w / me / omega(param_combis[:, 5])**2 / (4*np.pi*epsilon0) # ne = water density
param_combis[:, 10]= e * E0fromE(param_combis[:, 2]*ureg.second, param_combis[:, 1]*ureg.meter, param_combis[:,0]*ureg.joule).to("volt/meter").magnitude /me /omega(param_combis[:, 5]) /c0
param_combis[:, 11]= 8 * e * E0fromE(param_combis[:, 2]*ureg.second, param_combis[:, 1]*ureg.meter, param_combis[:,0]*ureg.joule).to("volt/meter").magnitude /(15.999*const.u-8*me) /omega(param_combis[:, 5]) /c0
param_combis[:, 12]= e * E0fromE(param_combis[:, 2]*ureg.second, param_combis[:, 1]*ureg.meter, param_combis[:,0]*ureg.joule).to("volt/meter").magnitude /mp /omega(param_combis[:, 5]) /c0

# Resample all entries with a0>a_top and a0<a_bottom
a_top = 40
a_bottom = 1# List of indices where a0>a_top
o40 = [i for i in range(len(param_combis)) if param_combis[:,10][i] > a_top or param_combis[:,10][i] < a_bottom]

# Loop to resample until no entries with a0>a_top or a0<a_bottom are left
while o40 != []:
    n_new = len(o40)
    param_combis = np.delete(param_combis, o40, axis=0)
    newparts = np.zeros((n_new, 13))
    newparts[:, 0] = uniform(np.sqrt(Parameter_Dict["Energy"][0].magnitude), np.sqrt(Parameter_Dict["Energy"][-1].magnitude), size=n_new)**2
    newparts[:, 1] = uniform(Parameter_Dict["Focus-FWHM"][0].to('meter').magnitude, Parameter_Dict["Focus-FWHM"][-1].to('meter').magnitude, size=n_new)
    newparts[:, 2] = uniform(Parameter_Dict["Pulselength"][0].to('second').magnitude, Parameter_Dict["Pulselength"][-1].to('second').magnitude, size=n_new)
    newparts[:, 3] = np.random.choice([0,1], size=n_new)
    newparts[:, 4] = uniform(Parameter_Dict["Theta_L"][0].magnitude, Parameter_Dict["Theta_L"][-1].magnitude, size=n_new)
    newparts[:, 5] = np.random.choice(Parameter_Dict["lambda_L"].to('meter').magnitude, size=n_new)
    newparts[:, 6] = np.random.choice(Parameter_Dict["d_T"].to('meter').magnitude, size=n_new)
    newparts[:, 7] = omega(newparts[:, 5])*newparts[:, 2]
    newparts[:, 8] = omega(newparts[:, 5])*newparts[:, 1]/c0
    newparts[:, 9] = e**2 * ne_w / me / omega(newparts[:, 5])**2 / (4*np.pi*epsilon0) # ne = water density
    newparts[:, 10]= e * E0fromE(newparts[:, 2]*ureg.second, newparts[:, 1]*ureg.meter, newparts[:,0]*ureg.joule).to("volt/meter").magnitude /me /omega(newparts[:, 5]) /c0
    newparts[:, 11]= 8 * e * E0fromE(newparts[:, 2]*ureg.second, newparts[:, 1]*ureg.meter, newparts[:,0]*ureg.joule).to("volt/meter").magnitude /(15.999*const.u-8*me) /omega(newparts[:, 5]) /c0
    newparts[:, 12]= e * E0fromE(newparts[:, 2]*ureg.second, newparts[:, 1]*ureg.meter, newparts[:,0]*ureg.joule).to("volt/meter").magnitude /mp /omega(newparts[:, 5]) /c0
    param_combis = np.append(param_combis, newparts, axis=0)
    o40 = [i for i in range(len(param_combis)) if param_combis[:,10][i] > a_top or param_combis[:,10][i] < a_bottom]



# Save to file

np.savetxt(str(args.outfile)+".txt", param_combis)
