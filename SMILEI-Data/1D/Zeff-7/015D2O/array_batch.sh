#!/bin/bash
#
#SBATCH --partition=main
#SBATCH --job-name=SmileiPIC1D
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --time=0-5
#

# SHELL SCRIPT READING THE PARAMETERS, STARTING THE SIMULATIONS AND RUNNING POST-PROCESSING

# get values from arguments
DIRPATH=$1

LUSTRE_HOME=/lustre/bhs/bschmitz

cd $LUSTRE_HOME/simulations2/$DIRPATH

IFS=$'\r\n' GLOBIGNORE='*' command eval 'SAMPLES=($(cat sampled_input_parameters.txt))'

# Step 1: Make folder for current sim
SIMDIR="Sim_${SLURM_ARRAY_TASK_ID}"

mkdir $SIMDIR

# Step 2: Copy namelist to simulation folder and apply parameters
NAMELIST='1dLorentzBoostTNSA.py'
cp ../$NAMELIST $SIMDIR

cd $LUSTRE_HOME/simulations2/$DIRPATH/$SIMDIR

PARAMS=(${SAMPLES[${SLURM_ARRAY_TASK_ID}]})

# Adding the lines in the namelist file
# sed -i "47s#.*#theta       = ${PARAMS[4]} * (np.pi/180.)#" "$NAMELIST"
# sed -i "59s#.*#lambdal     = ${PARAMS[5]} * gamma0#" "$NAMELIST"
# sed -i "80s#.*#plasmathick = ${PARAMS[6]}/refl#" "$NAMELIST"
# sed -i "87s#.*#tl          = ${PARAMS[2]}/reft#" "$NAMELIST"
# sed -i "94s#.*#aL          = ${PARAMS[10]}#" "$NAMELIST"
# sed -i "97s#.*#laserpolid  = ${PARAMS[3]}#" "$NAMELIST"

# Replacing the parameter values

sed -i -r\
    -e "s#^theta *=.*#theta       = ${PARAMS[4]} * (np.pi/180.)#" \
    -e "s#^lambdal *=.*#lambdal     =  ${PARAMS[5]} * gamma0#" \
    -e "s#^plasmathick *=.*#plasmathick = ${PARAMS[6]}/refl#" \
    -e "s#^tl *=.*#tl          = ${PARAMS[2]}/reft#" \
    -e "s#^aL *=.*#aL          = ${PARAMS[10]}#" \
    -e "s#^laserpolid *=.*#laserpolid  = ${PARAMS[3]}#" "$NAMELIST"

# Step 3: Run simulation
$LUSTRE_HOME/smilei-wrapper singularity exec --bind $LUSTRE_HOME $LUSTRE_HOME/smilei.sif smilei $LUSTRE_HOME/simulations2/$DIRPATH/$SIMDIR/$NAMELIST

# Step 4: Run post-processing
singularity exec --bind $LUSTRE_HOME $LUSTRE_HOME/smilei.sif python3 $LUSTRE_HOME/simulations2/post-process.py -n ${SLURM_ARRAY_TASK_ID}

# Step 5: Delete everything except log and move log to upper folder
read NEWLINE THROWAWAYNAME <<< $(wc -l $LUSTRE_HOME/simulations2/inputoutput-H.csv)
cd $LUSTRE_HOME/simulations2/$DIRPATH
cp slurm-${SLURM_JOB_ID}_${SLURM_ARRAY_TASK_ID}.out $LUSTRE_HOME/simulations2/logs/
mv $LUSTRE_HOME/simulations2/logs/slurm-${SLURM_JOB_ID}_${SLURM_ARRAY_TASK_ID}.out $LUSTRE_HOME/simulations2/logs/log${NEWLINE}.out
rm -r $SIMDIR
