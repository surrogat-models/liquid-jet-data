# Liquid-Jet-Data

[![License: Apache 2.0](https://img.shields.io/badge/License-cc-Attribution4.svg)](https://git.rwth-aachen.de/surrogat-models/liquid-jet-data/-/blob/main/LICENSE)
<!-- [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6383868.svg)](https://doi.org/10.5281/zenodo.6383868) -->



## Table of contents
1. [Introduction](#introduction)
2. [Strucuture of Raw Data Files](#rawstructure)
3. [Strucuture of Folder Tree](#folderstructure)
4. [Known Problems](#bugs)
5. [Publications & Links](#publications)
6. [Contributers](#contributors)
7. [Funding](#funding)

## 1. Introduction <a name="introduction"></a>

This repository contains the data for the liquid jet Particle-In-Cell simulations.
It consists of the simulations scripts, the individual raw data, the postprocessing script and the joined data files.

The data was calculated using the SMILEI simulation tool [1] using the VIRGO HPC cluster [2]. 
This data is the basis for the liquid jet modeling paper [3].
It is part of the LOEWE NP research cluster at TU Darmstadt [4] and the work was done at the Fachgebiet Beschleunigerphysik at TU Darmstadt [5]


## 2. Structure of raw data files <a name="rawstructure"></a>

The individual simulations are saved in tables (.csv) and have 114 data columns. 
They consists of the simulation parameters and the actual data as follows: 

| Parameters | Data Spectrum | Data Emax | 
| ---         | ---           | ---       |
| Parameters used for the simulation, created during runtime of `make_samples.py` | Normalized data to project spectrum onto 100 bins.  | Maximal energy to normalize spectrum to. |
| 13 Parameters: 0-Energy, 1-Focus-FWHM, 2-Pulselength, 3-Polarisation, 4-Theta_L, 5-lambda_L 6-Target-Thickness, 7-Pi 1, 8-Pi_2, 9-Pi_3e, 10-Pi_4e, 11-Pi_40, 12-Pi_4p | 100 data bins sorted from lowest to highest energy| 1 data point with the Emax in MeV |

The output files, denoted with 'fulldata- inputoutput' have two additional columns, giving additional metadata for the simulations, which are extractd from the folder tree.

| Meta | Parameters | Data Spectrum | Data Emax | 
| ---  | ---         | ---           | ---       |
| Meta data created during runtime of `prepareRawData.py`  | Parameters used for the simulation, created during runtime of `make_samples.py` | Normalized data to project spectrum onto 100 bins.  | Maximal energy to normalize spectrum to. |
| 2 Parameters: 0-Oxygen-Zeff; 1-Mixture, percentage of Deuterium in the system  | 13 Parameters: 0-Energy, 1-Focus-FWHM, 2-Pulselength, 3-Polarisation, 4-Theta_L, 5-lambda_L 6-Target-Thickness, 7-Pi 1, 8-Pi_2, 9-Pi_3e, 10-Pi_4e, 11-Pi_40, 12-Pi_4p | 100 data bins sorted from lowest to highest energy| 1 data point with the Emax in MeV |

## 3. Structure of Folder Tree <a name="folderstructure"></a>

The data is organized in a tree: `./SMILEI-Data/xD/Zeff-y/zzzD20/`
* x indicates the dimensionality of the simulations 
* y the ionizationlevel of the Oxygen 
* zzz the three digit percentage of D20 in the plasma.

Most folders further contain files to deploy the simulations on the VIRGO cluster:
* `array_batch.sh`: Starts and controls simulation on the cluster.
* `make_samples.py`: Calculates randomized variable combinations.
* `post_process.py`: Postprocessing to apply lorentz back transform and create data CSVs.
* `sim_run.py`: Join all above together and queue simulations.

## 4. Known Problems <a name="bugs"></a>
The calculation of the Pi values in the postprocessing method is not entirely correct. 
It is recommended to discard those values and - if needed - recalculate them based on the physical quantities.
Modeling based on the physical parameters is more stable than on Pi values, due to the Pis being not independent anymore. 
The Pi are compositions of the physical parameters following the construction sketched in the paper [3]

## 5. Publications & Links <a name="publications"></a>

[1] [SMILEI PIC Code](https://smileipic.github.io/Smilei/index.html)

[2] [VIRGO HPC Cluster](https://hpc.gsi.de/virgo/preface.html)

[3] [Modeling of a Liquid Leaf Target TNSA Experiment Using Particle-In-Cell Simulations and Deep Learning](https://doi.org/10.1155/2023/2868112)

[4] [LOEWE center for Nuclear Photonics](https://www.ikp.tu-darmstadt.de/nuclearphotonics/nuclear_photonics/index.en.jsp) 

[5] [Accelerator Pyhsics at TEMF, TU Darmstadt](https://www.bp.tu-darmstadt.de/fachgebiet_beschleunigerphysik/index.en.jsp)

## 6. Contributors <a name="contributors"></a>
Contributors include (alphabetically): 
*   D. Kreuter, 
*   B. Schmitz

## 7. Funding <a name="funding"></a>
* This work was funded by HMWK through the LOEWE center “Nuclear Photonics”.
* The simulations, were performed on the Virgo HPC cluster at the GSI Helmholtzzentrum für Schwerionenforschung, Darmstadt (Germany) in the frame of FAIR Phase-0.
