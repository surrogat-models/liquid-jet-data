#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May  1 18:38:26 2022

@author: benedikt
"""
#%% Step 1: Import modules
import pandas as pd 
import os
import tqdm

#%% Step 2: Define Data Repository and Import
## 1D or 2D Simulation?
dim = ['1D/','2D/']

# which dimension to take?
# position in above list: 0 = 1D, 1 = 2D
d=0 
DataPath = './SMILEI-Data/'+dim[d]

# particle Species:
particles = ['inputoutput-D.csv','inputoutput-H.csv','inputoutput-O.csv','inputoutput-e.csv']

# Main loop over the particle species
for part in tqdm.tqdm(particles): 
    # Preallocation to save the imported dataframes to a list, to concatenated afterwards
    list_of_dataframes = []
    
    # reassign the path value to not get endless paths
    DataPath = './SMILEI-Data/'+dim[d]
    # find the different Z values:
    Zets = os.listdir(DataPath)
    for Zeff in Zets:
        # Convert the Zeff from string to float.
        ZeffValue = float(Zeff[5:])
        # adjust file path
        ZPath = DataPath+Zeff+'/'
        # Data is saved according to its D2O to H2O ratio
        mixtures = os.listdir(ZPath)
        for mix in mixtures:
            if mix != 'readme.txt':
                tempMix = float(mix.split('D')[0])/100    
                # Import csv as pandas dataframe
                # if is important since there is a case were no deuterium is present in the system:
                # check whether file exists before joining data:
                FullPath = ZPath+mix+'/'+part
                    
                os.path.exists(FullPath)
                if os.path.exists(FullPath):
                    tempDF = pd.read_csv(FullPath, header=None)
                    # set up a new series with the length of the dataframe
                    length_of_dataframe = tempDF.shape[0]
                    mixVariable = [tempMix for i in range(length_of_dataframe)]
                    # add mix variable in the first column (column 0)
                    tempDF.insert(0,None,mixVariable)
                    # add Zeff to DataFrame
                    ZeffVariable = [ZeffValue for i in range(length_of_dataframe)]
                    tempDF.insert(0,None,ZeffVariable)
                    list_of_dataframes.append(tempDF)
            Export = pd.concat(list_of_dataframes)
            # Save data to file
            Export.to_csv('fulldata-'+part)